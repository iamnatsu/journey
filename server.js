const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const database = require('./util/db');
const Routes = require('./app/routes/routes');
const AuthRoutes = require('./app/routes/Auth.routes');
app.use(cors());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

//Routes
// app.all('/backend*', [require('./app/middleware/jwtvalidator.middleware')]);
app.use('/', Routes);
app.use('/', AuthRoutes);
//Routes


app.listen(9000, () => {
    console.log("App is runing on port 9000");
})