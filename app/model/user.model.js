const mongoose = require('mongoose');

const Users = mongoose.Schema({
    username: String,
    password: String,
    userdata: [{
        date: Date,
        imageurl: [],
        notes: String,
        title: String
    }]
})

module.exports = mongoose.model('users', Users);