const crypto = require('crypto');
const UsersModel = require('../model/user.model');
var randomstring = require("randomstring");
const jwt = require('jsonwebtoken');
//Function to encrypt
// function encrypt(text) {
//     var randomNUmber = randomstring.generate({
//         length: 32,
//         charset: 'alphabetic'
//     });
//     const ENCRYPTION_KEY = randomNUmber;
//     const iv = crypto.randomBytes(16);
//     var encryptKey = ENCRYPTION_KEY;
//     let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), iv);
//     let encrypted = cipher.update(text);
//     encrypted = Buffer.concat([encrypted, cipher.final()]);
//     return iv.toString('hex') + ':' + encrypted.toString('hex') + ':' + encryptKey;
// }

// function decrypt(text) {
//     let textParts = text.split(':');
//     var splitedData = textParts[0] + ':' + textParts[1];
//     splitedData = splitedData.split(':');
//     let iv = Buffer.from(splitedData.shift(), 'hex');
//     let encryptedText = Buffer.from(splitedData.join(':'), 'hex');
//     let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(textParts[2]), iv);
//     let decrypted = decipher.update(encryptedText);
//     decrypted = Buffer.concat([decrypted, decipher.final()]);
//     return decrypted.toString();
// }

var genRandomString = function (length) {
    return crypto.randomBytes(Math.ceil(length / 2))
        .toString('hex') /** convert to hexadecimal format */
        .slice(0, length); /** return required number of characters */
};
var sha512 = function (password, salt) {
    var hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hash.update(password);
    var value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};

function saltHashPassword(userpassword) {
    var salt = genRandomString(16); /** Gives us salt of length 16 */
    var passwordData = sha512(userpassword, salt);
    return ({
        passwordHash: passwordData.passwordHash,
        salt: passwordData.salt
    });
}

exports.Signin = (req, res) => {
    var username = req.body.username;
    var password = req.body.password;
    if (username == "" || password == "") {
        res.jsonp({
            "Result": "username or password are empty"
        })
    } else {
        UsersModel.find({
            username: username
        }).then(result => {
            if (result.length == 0) {
                var encyptedPassword = JSON.stringify(saltHashPassword(password));
                const user = new UsersModel({
                    username: username,
                    password: encyptedPassword
                });
                user.save().then(result => {
                    res.jsonp({
                        "Result": result
                    })
                }).catch(error => {
                    res.jsonp({
                        "Result": error
                    })
                })
            } else {
                res.jsonp({
                    "Result": "User already exits"
                })
            }
        }).catch(err => {
            res.jsonp({
                "Result": err
            })
        })
    }

}

exports.Login = async (req, res) => {
    var username = req.body.username || '';
    var password = req.body.password || '';
    if (username == '' || password == '') {
        res.status(401);
        res.jsonp({
            "Message": "Username or password is empty"
        });
        return;
    }

    var dbUserObject = await validate(username, password);
    console.log(dbUserObject);
    if (!dbUserObject.success) {
        res.status(401);
        res.jsonp({
            "Result": dbUserObject
        });
        return;
    }
    if (dbUserObject.success) {
        res.jsonp(dbUserObject);
    }
}

const validate = (username, password) => {
    var user = username;
    var pass = password;
    return new Promise(function (resolve) {
        UsersModel.find({
            username: username
        }).then(async (result) => {
            if (result.length == 0) {
                resolve({
                    success: false,
                    Result: "User does not exit, sigin in to login"
                })
            } else {
                var Resultpass = JSON.parse(result[0].password);
                var decyptedPassword = await sha512(pass, Resultpass.salt);
                if (Resultpass.passwordHash == decyptedPassword.passwordHash) {
                    const token = genToken({
                        username: username
                    })
                    resolve({
                        statuscode: 200,
                        success: true,
                        token: token
                    })
                } else {
                    resolve({
                        success: false,
                        message: 'Username or password is Incorrect'
                    })
                }
            }
        }).catch(err => {
            resolve({
                success: false,
                Result: err
            })
        })
    })
}

const genToken = (user) => {

    var token = jwt.sign({
        username: user.username
    }, `"process.env.secret_key"`, {
        expiresIn: 86400
    });
    return token;
}