const express = require('express');
const router = express.Router();
const CRUDJourneyController = require('../controller/crudJourney.controller');

router.post('/userData', CRUDJourneyController.test);


module.exports = router;