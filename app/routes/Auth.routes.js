const express = require('express');
const router = express.Router();
const userController = require('../controller/user.controller');

router.post('/sigin', userController.Signin);
router.post('/login', userController.Login);


module.exports = router;