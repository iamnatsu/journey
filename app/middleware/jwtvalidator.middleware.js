const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {

    var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];
    var key = (req.body && req.body.x_key) || (req.query && req.query.x_key) || req.headers['x-key'];
    if (token || key) {
        try {
            jwt.verify(token, "process.env.secret_key", async function (err, decoded) {
                if (err) {
                    console.log(err);
                    if (err.name == "JsonWebTokenError") {
                        return res.status(401).send({
                            auth: false,
                            message: 'Invalid token'
                        });
                    } else if (err.name == "TokenExpiredError") {
                        return res.status(401).send({
                            auth: false,
                            message: 'Token expired'
                        });
                    } else {
                        return res.status(401).send({
                            auth: false,
                            message: 'failed to authenticate token'
                        });
                    }
                } else {
                    console.log('decoded', decoded);
                    var dbUser = await validateUser(key, decoded);
                    console.log(dbUser);
                    if (dbUser) {
                        if (dbUser.success) {
                            next(); // To move to next middleware
                        } else {
                            res.status(401);
                            res.json({
                                "status": 401,
                                "message": "Not Authorized"
                            });
                            return;
                        }
                    } else {
                        res.status(401);
                        res.json({
                            "status": 401,
                            "message": "Invalid User"
                        });
                        return;
                    }
                }
            });
        } catch (err) {
            res.status(500);
            res.json({
                "status": 500,
                "message": "Oops something went wrong",
                "error": err
            });
        }
    } else {
        res.status(401);
        res.json({
            "status": 401,
            "message": "Unauthorised!! missing authentication token "
        });
        return;
    }
}

validateUser = (id, data) => {
    // spoofing the DB response for simplicity
    return new Promise(function (resolve) {

        if (id == data.username) {
            resolve({
                success: true
            })
        } else {
            resolve({
                success: false
            })
        }
    })
}